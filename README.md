# README #

Ejemplos que usan la interface sockets en Linux, basados en los ejemplos del capítulo de libro [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/3e/home.html)

### Compilación ###

* Compilar el cliente: 
    * Entrar en la carpeta 'cliente'
	* $ make client

* Compilar el servidor:
    * Entrar en la carpeta 'servidor'
	* $ make server

* Compilar todo:
    * Desde el menú principal ejecutar '$ make'
	
### Uso ###
Ejecutar el cliente:

```
./client <host> <port>
```
Ejemplo:

```
./client 127.0.0.1 5000
```

Ejecutar el cliente:

```
./server <port>
```
Ejemplo:

```
./server 5000
```
