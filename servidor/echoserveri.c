#include "csapp.h"
#define FILE_TO_SEND "./file2send.txt"

void echo(int connfd);

int main(int argc, char **argv){
    int listenfd, connfd;
    unsigned int clientlen;
    struct sockaddr_in clientaddr;
    struct hostent *hp;
    char *haddrp, *port;

////////////////////
    ssize_t len;
    int fd;
    struct stat file_stat;
    int sent_bytes = 0;
    char file_size[256];
//    int remain_data;
////////////////////

    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(0);
    }
    port = argv[1];

    listenfd = Open_listenfd(port);
    while (1) {
        clientlen = sizeof(clientaddr);
        connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

        /* Determine the domain name and IP address of the client */
        hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
                    sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        haddrp = inet_ntoa(clientaddr.sin_addr);
        printf("\n > Server connected to %s (%s)\n", hp->h_name, haddrp);
/*--------------------------------------------------------*/
        // Open the file to be sent */
        fd = open(FILE_TO_SEND, O_RDONLY);
        if (fd == -1) {
            fprintf(stderr, "Error al abrir el archivo '%s': %s\n", FILE_TO_SEND, strerror(errno));
            send((int)connfd, "-1", 2, 0);
            printf("\n > Conexion terminada. \n-------------------------- \n");
            Close(connfd);
            
        }
        // Get the stat of the file to be sent */
        if ( fstat(fd, &file_stat) < 0 ) {
            fprintf( stderr, "Error fstat --> %s", strerror(errno) );
            send( (int)connfd, "-1", 2, 0 );
            printf("\n > Conexion terminada. \n-------------------------- \n");
            Close(connfd);
        }
        sprintf(file_size, "%d", file_stat.st_size);

        // Sending file size */
        len = send((int)connfd, file_size, 5, 0);
        if (len < 0) {
            fprintf(stderr, "Error al enviar el tamaño del archivo --> %s\n", strerror(errno));
            printf("\n > Conexion terminada. \n-------------------------- \n");
            Close(connfd);
        }

        /* Copy file using sendfile file_stat.st_size */
        printf("\n   > Enviando archivo...");
//        remain_data = file_stat.st_size;
        sent_bytes = sendfile( (int)connfd, fd, NULL, file_stat.st_size );

        if (sent_bytes == -1) {
            fprintf(stderr, "Error from sendfile: %s\n", strerror(errno));
            printf("\n > Conexion terminada. \n-------------------------- \n");
            Close(connfd);
        }

        if ( sent_bytes != file_stat.st_size ) {
            fprintf( stdout, "Transfering file: %d of %d bytes\n",
                     sent_bytes,
                     (int)file_stat.st_size);
        }
        printf("\n   > Final del envio.");
        //Close(connfd);
        printf("\n   > Conexion terminada. \n---------------------------\n");
        
        /* close descriptor for file that was sent */
        //close(fd);
        /* close socket descriptor */
        //Close(connfd);
        //exit(EXIT_FAILURE);
        

/*--------------------------------------------------------*/


    }

    //Close(connfd);
    exit(0);
}


void echo(int connfd) {
	size_t n;
	char buf[MAXLINE];
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("server received %lu bytes\n", n);
		Rio_writen(connfd, buf, n);
	}
}
