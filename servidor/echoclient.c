#include "csapp.h"

#define FILENAME        "./myfile.txt"

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
    
    ssize_t len;
    struct sockaddr_in remote_addr;
    int file_size;
    FILE *received_file;
    int remain_data = 0;

	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
printf("here1\n");

	while (Fgets(buf, MAXLINE, stdin) != NULL) {
printf("here2\n");
		Rio_writen(clientfd, buf, strlen(buf));
		Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);
	}
	
printf("here3\n");
    recv(host, buf, MAXLINE, 0);
    file_size = atoi(buf);
    fprintf(stdout, "\nTamaño de archivo: %d\n", file_size);
printf("here4\n");

    received_file = fopen(FILENAME, "w");
    if (received_file == NULL) {
        fprintf(stderr, "Error al abrir.. %s\n", strerror(errno));
        //exit(EXIT_FAILURE);
        Close(clientfd);
        exit(0);
    }

    remain_data = file_size;
    len = recv(host, buf, MAXLINE, 0);
    while (( len >= 0) && (remain_data > 0)) {
        fwrite(buf, sizeof(char), len, received_file);
        remain_data -= len;
        fprintf(stdout, "Recibe %d bytes y se espera:- %d bytes\n", len, remain_data);
    }

    fclose(received_file);
    Close(clientfd);
    exit(0);
}
