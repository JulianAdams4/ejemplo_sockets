
Puedo escribir los versos más tristes esta noche. Escribir, por ejemplo: "La noche está estrellada, y tiritan, azules, los astros, a lo lejos". Puedo escribir los versos más tristes esta noche. Pensar que no la tengo. Sentir que la he perdido. Oir la noche inmensa, más inmensa sin ella. Y el verso cae al alma como al pasto el rocío. Qué importa que mi amor no pudiera guardarla. La noche está estrellada y ella no está conmigo.

