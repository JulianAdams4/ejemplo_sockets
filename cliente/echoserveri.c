#include "csapp.h"
#define FILE_TO_SEND "./file2send.txt"

void echo(int connfd);

int main(int argc, char **argv){
    int listenfd, connfd;
    unsigned int clientlen;
    struct sockaddr_in clientaddr;
    struct hostent *hp;
    char *haddrp, *port;

////////////////////
    ssize_t len;
    int fd;
    struct stat file_stat;
    int sent_bytes = 0;
    char file_size[256];
    int offset;
    int remain_data;
////////////////////

    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(0);
    }
    port = argv[1];

    listenfd = Open_listenfd(port);
    while (1) {
        clientlen = sizeof(clientaddr);
        connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

        /* Determine the domain name and IP address of the client */
        hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
                    sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        haddrp = inet_ntoa(clientaddr.sin_addr);
        printf("\n > Server connected to %s (%s)\n", hp->h_name, haddrp);

///////////////////////////////////
        /* Abrimos el archivo */
        fd = open(FILE_TO_SEND, O_RDONLY);
        if (fd == -1) {
            fprintf(stderr, "Error al abrir el archivo\n-> %s\n", strerror(errno));
            send((int)connfd, "-1", 2, 0);
            Close(connfd);			
        }
        /* Get file stats */
        if (fstat(fd, &file_stat) < 0) {
            fprintf(stderr, "Error fstat --> %s", strerror(errno));
            send((int)connfd, "-1", 2, 0);
            Close(connfd);
            //exit(EXIT_FAILURE);
        }
        
        fprintf(stdout, "\n > Tamaño del archivo a enviarse: \t %d bytes\n", (int)file_stat.st_size);
        sprintf(file_size, "%d", file_stat.st_size);

        /* Sending file size */
        len = send((int)connfd, file_size, sizeof(file_size), 0);
        if (len < 0) {
            fprintf(stderr, "Error on sending greetings --> %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        fprintf(stdout, " > Server sent %d bytes for the size\n", len);

        offset = 0;
        remain_data = file_stat.st_size;
        /* Sending file data */
        //sent_bytes = sendfile((int)connfd, fd, &offset, BUFSIZ);
        sent_bytes = sendfile((int)connfd, fd, NULL, BUFSIZ);

//printf("Remain data = %d\n", (int)remain_data );
//printf(" Sent bytes = %d\n", (int)sent_bytes );
        
        while ( sent_bytes >= 0 && remain_data > 0 ) {
            fprintf(stdout, "1. El servidor envió %d bytes del archivo. \n   Datos pendientes = %d bytes\n", sent_bytes, remain_data);
            remain_data -= sent_bytes;
            fprintf(stdout, "2. El servidor envió %d bytes del archivo. \n   Datos pendientes = %d bytes\n", sent_bytes, remain_data);
            //fprintf(stdout, "2. Server sent %d bytes from file's data, offset is now : %d and remaining data = %d\n", sent_bytes, offset, remain_data);
        }
///////////////////////////////////
        //echo(connfd);
        Close(connfd);
        exit(0);
    }
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("server received %lu bytes\n", n);
		Rio_writen(connfd, buf, n);
	}
}
