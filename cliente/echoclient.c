#include "csapp.h"

#define FILENAME "./receivedfile.txt"

int main(int argc, char **argv) {
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
    
    ssize_t len;
    struct sockaddr_in remote_addr;
    int file_size;
    FILE *received_file;
    int remain_data = 0;
    int i = 0;

	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);

//	while (Fgets(buf, MAXLINE, stdin) != NULL) {
	while (1) {
		Rio_writen(clientfd, buf, strlen(buf));
		Rio_readlineb(&rio, buf, MAXLINE);

        if ( (int)buf == -1 ) {
            printf("\n > Archivo no encontrado. No se pudo realizar la trasferencia.\n > Cerrando conexion.\n");
            Close(clientfd);
            exit(0);
        }
        else {
            if ( i == 0 ) {
                printf("\n------------------------------------");
                printf("\n > Tamaño de archivo a recibir: %s bytes.\n\t", buf );
                len = (ssize_t) buf;
                file_size = atoi(buf);
                i++;                
            }
            else {
                buf[file_size] = '\0';
                printf("%s\n", buf );
                printf("\n > Escribiendo archivo...");
                received_file = fopen(FILENAME, "w");
                if (received_file == NULL) {
                    fprintf(stderr, " > Error al escribir el archivo: %s\n", strerror(errno));
                    Close(clientfd);
                    exit(0);
                }
                // Escribimos el archivo */
                fwrite(buf, 1, sizeof(buf), received_file);
                fclose(received_file);
                Close(clientfd);
                printf("\n > Envio terminado.\n > Conexion terminada.\n------------------------------------\n");
                exit(0);
            }

        }

	}

}
